/**************************
 * Default Parameter
 *{
		containerId : null,
		src: null,
		poster:null,
		track:{
			src:null,
			language:null,
			kind:null,
			label:null
		},
		ad:{
			url:null,
			cue:null
		},
		drm:{
			widevine:{
				src:null,
				headers:[{
					name:null,
					value:null
				}],
				licenceurl:null
			},
			playready:{
				src:null,
				headers:[{
					name:null,
					value:null
				}],
				licenceurl:null
			},
			fairplay:{
				src:null,
				headers:[{
					name:null,
					value:null
				}],
				licenseurl:null,
				certificateurl:null
			}
		}
*};
*
***************************/


(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('video.js')) :
	typeof define === 'function' && define.amd ? define(['video.js'], factory) :
	(global.videojsHttpSourceSelector = factory(global.videojs));
}(this, (function (videojs) { 
	
    'use strict';
    var JTPlayer = window.JTPlayer || {};

    JTPlayer = (function() {

        function JTPlayer(settings) {
            var _this = this;
            _this.originalOptions = settings;
            _this.defaults = {
                    appName : "jtvPlayer",
                    appVer : "0.0.1",
                    playerId : "videojs-jtv-streaming-player",
            };
            _this.options = extendOption(_this.defaults, settings);
            function extendOption(defOptObj, userOptObj) {
                for (var ind in userOptObj) {
                    if (userOptObj.hasOwnProperty(ind)) {
                        defOptObj[ind] = userOptObj[ind];
                    }
                }
                return defOptObj;
            }
            _this.initialize();
        }
        return JTPlayer;
    }());

    JTPlayer.prototype.initialize = function () {
            this.createPlayerContainer();
    };
	
    JTPlayer.prototype.getPosition = function getPosition() {
        var _player = this.getPlayerContainer();
        if(!this.isUBN(_player)){
                var _curr = _player.currentTime();
                return _curr;
        }
    };

    JTPlayer.prototype.getDuration = function getDuration() {
        var _player = this.getPlayerContainer();
        if(!this.isUBN(_player)){
            var _dur = _player.duration();
            return _dur;
        }
    };
	
    JTPlayer.prototype.getVisualQuality = function getVisualQuality() {
        var _player = this.getPlayerContainer();
        if(!this.isUBN(_player)){
            var _quality = _player.qualityLevels();
            if(_quality){
                return _quality[_quality.selectedIndex];
            }
        }
    };

    JTPlayer.prototype.stop = function stop() {
        var _player = this.getPlayerContainer();
        if(!this.isUBN(_player)){
            _player.reset();
            this.configPlayer();
        }
    };
    
    JTPlayer.prototype.stop = function dispose() {
        var _player = this.getPlayerContainer();
        if(!this.isUBN(_player)){
            _player.dispose();
        }
    };

    JTPlayer.prototype.seek = function seek(_time) {
        if(!_time)
            return;
        var _player = this.getPlayerContainer();
        if(!this.isUBN(_player)){
            _player.currentTime(_time);
        }
    };

    JTPlayer.prototype.play = function play() {
        var _player = this.getPlayerContainer();
        if(!this.isUBN(_player)){
            _player.play();
        }
    };

    JTPlayer.prototype.pause = function pause() {
        var _player = this.getPlayerContainer();
        if(!this.isUBN(_player)){
            _player.pause();
        }
    };
    
    JTPlayer.prototype.requestFullscreen = function requestFullscreen() {
        var _player = this.getPlayerContainer();
        if(!this.isUBN(_player)){
            _player.requestFullscreen();
        }
    };

    JTPlayer.prototype.on = function on(event, fn) {
        if(!event)
                return;
        var _this = this, _player = this.getPlayerContainer();
        if(!_this.isUBN(_player)){
            _player.on(event, function(res){
                if(!_this.isUBN(fn)){
                        fn(res);
                }
            });
        }
    };
	
    JTPlayer.prototype.off = function off(event) {
        if(!event)
            return;
        var _player = this.getPlayerContainer();
        if(!this.isUBN(_player)){
        	_player.off(event);
        }
    };
	
    JTPlayer.prototype.createPlayerContainer = function () {
        var _container = this.getContainerId(),
        _playercontainer = this.getPlayerId();
        var cntr_ele = document.getElementById(_container);
        cntr_ele.innerHTML = '<video-js id="'+_playercontainer+'" class="vjs-default-skin vjs-16-9 vjs-big-play-centered"></video-js>';
        this.createPlayer();
    };
	
    JTPlayer.prototype.createPlayer = function () {
        var _this = this, 
        _playercontainer = this.getPlayerId(),
        _op = this.getPlayerOption();

        if(videojs){
            var player = videojs(_playercontainer, _op);
            this.player = player;
            var vpd = document.getElementsByClassName("vjs-playedtime-duration")[0];
            if(vpd)
                vpd.parentNode.removeChild(vpd);
            var vrtd = document.getElementsByClassName("vjs-remaining-time-display")[0];
            vrtd.insertAdjacentHTML("afterend", '<span class="vjs-playedtime-duration"><span class="vjs-jtv-curtime-time-display">0:00:00 </span> / <span class="vjs-jtv-duration-time-display">0:00:00<span></span>');
            player.ready(function(){
                _this.configPlayer();
                _this.resizePlayer();
            });
        }
    };
	
    JTPlayer.prototype.getPlayerOption = function () {    	
    	var _op = {
            html5: {
                hls: {
                  overrideNative: !videojs.browser.IS_SAFARI
                },
                nativeTextTracks: false
            },
            textTrackSettings: false,
            controlBar: {
                volumePanel: {inline: false}
            },
            "controls": true, 
            "autoplay": true, 
            "preload": "auto",
            "responsive": true
         };
    	
    	 return _op;
    };
    
    JTPlayer.prototype.configPlayer = function () {    	
    	var _player = this.getPlayerContainer();
        var _this =this;
    	
    	if(!this.isUBN(_player)){
    		if(this.isDRM()){
    			if(videojs.browser.IS_SAFARI){
    				var _sc = this.getConfigFairPlay();
    				_sc['player'] = _player;
    				JTVFairplay.fairplay(_sc);
    			}else{
    				var _sc = this.getConfigDRMPlay();
    				_player.eme();
    	       	    _player.src(_sc);
    	       	    _player.httpSourceSelector();
    			}
    		}else{
    			var _sc = this.getConfigAllPlay();
    			_player.src(_sc);
    			_player.httpSourceSelector();
    		}
    		
    		var _poster = this.getPoster();
    		if(!this.isUBN(_poster)){
    			_player.poster(_poster);
    		}
    		_player.on("loadedmetadata", function () {
                _this.showPlayerDuration();
            });
            _player.on("timeupdate", function () {
                _this.updateCurrentTime();
            });
    		this.loadTrack();
    		this.loadAd();
    	}
    };
    JTPlayer.prototype.showPlayerDuration = function () {
        var _player = this.getPlayerContainer();
        if(!this.isUBN(_player)){
	        var dur=_player.duration();
                var vpc = document.getElementsByClassName("vjs-progress-control")[0];
	        if(dur != "Infinity"){
	            dur= this.secondsToHms(dur);
                var vrtd = document.getElementsByClassName("vjs-remaining-time-display")[0];
                vrtd.style.display ="none";
                var vjdtd = document.getElementsByClassName("vjs-jtv-duration-time-display")[0];
                vjdtd.innerHTML=dur;
                vpc.style.opacity = 1;
	        }else{
                vpc.style.opacity = 0;
	        }
        }
    };
    JTPlayer.prototype.updateCurrentTime = function () {
        var _player = this.getPlayerContainer();
        var ctime = this.secondsToHms(_player.currentTime());
        var vpc = document.getElementsByClassName("vjs-progress-control")[0];
        var vrtd = document.getElementsByClassName("vjs-remaining-time-display")[0];
        var vjctd = document.getElementsByClassName("vjs-jtv-curtime-time-display")[0];
        vrtd.style.display ="none";
        vjctd.innerHTML=ctime;
        var dur=_player.duration();
        if(dur == "Infinity"){
            vpc.style.opacity = 0;
        }else{
            vpc.style.opacity = 1;
        }
    };
    
    JTPlayer.prototype.loadTrack = function () {    	
    	var _player = this.getPlayerContainer();
    	
    	if(!this.isUBN(_player)){
    		var _track = this.getTrack();
    		if(_track != null){
                    if(typeof _track == 'object'){
                    	try{
	                        _track['id'] = 'vjs_jtv_subtitle';
	                        _player.one("loadedmetadata", function () {
	                            var trackEl = _player.addRemoteTextTrack(_track, false);
	                            trackEl.addEventListener('load', function() {
	                                var tracks = _player.textTracks();
	                                for (var i = 0; i < tracks.length; i++) {
	                                  var track = tracks[i];
	                                  if(track.kind == 'metadata' || track.id == 'vjs_jtv_subtitle')
	                                          continue;
	                                  _player.removeRemoteTextTrack(track);
	                                }
	                            });
	                        });
                    	}catch(err){
                    		console.log(err)
                    	}
                    }
                }
    	}
    };
    
    JTPlayer.prototype.loadAd = function () {    	
    	var _player = this.getPlayerContainer();
    	
    	if(!this.isUBN(_player)){
    		var _ad = this.getAd();
    		if(_ad != null){
    			if(typeof _ad == 'object'){
    				if(!this.isUBN(_ad['url']) && 
    					!this.isUBN(_ad['cue'])){
    					
    					try{
	    					var _adurl = _ad['url'],
	    					_adcue = (typeof _ad['cue'] == "string")?_ad['cue'].split(","):_ad['cue'],
	    					vid = _player.id();
	    					
	    					var purl = encodeURIComponent(window.location.href);
	    				    var cb = new Date().getTime();
	    				    _adurl = _adurl.replace('__page-url__', purl);
	    				    _adurl = _adurl.replace('__random-number__', cb);
	    				    _adurl = _adurl.replace('__player-width__', _player.width());
	    				    _adurl = _adurl.replace('__player-height__', _player.height());
	    					
	    					_adcue=_adcue.map(function(v){ return Math.round(v); });
	                                        
	    		            var marker_arr=[];
	    		            for(var cp in _adcue){
	    		                var cpobj= {time : _adcue[cp],text:""};
	    		                marker_arr.push(cpobj);
	    		            }
	    		            _player.markers({
	    		                markers: marker_arr
	    		            });
	    		            
	    		            var options = {
			                    id: vid,
			                    adTagUrl:_adurl
			                };
			                _player.ima(options);
			                
			                var adplayed_cp=[];
			                _player.on("timeupdate",function(e){
	                               var ct = Math.round(_player.currentTime()); // in sec 
	                               if(_adcue.indexOf(ct) > -1 && adplayed_cp.indexOf(ct) === -1){
			                       adplayed_cp.push(ct);
			                       _player.ima.requestAds();
			                   }
			                });
			                
			                // Initialize the ad container when the video player is clicked, but only the
			                // first time it's clicked.
			                var initAdDisplayContainer = function() {
			                  //console.log("ad init");
			                  _player.ima.initializeAdDisplayContainer();
			                  wrapperDiv.removeEventListener(startEvent, initAdDisplayContainer);
			                }
	
			                var startEvent = 'click';
			                if (navigator.userAgent.match(/iPhone/i) ||navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/Android/i)) {
			                  startEvent = 'touchend';
			                }
	
			                var wrapperDiv = document.getElementById(vid);
			                wrapperDiv.addEventListener(startEvent, initAdDisplayContainer);
    					}catch(err){
    						console.log(err);
    					}
    				}
        		}
    		}
    	}
    };
    
    JTPlayer.prototype.resizePlayer = function () {    	
    	var _player = this.getPlayerContainer();
    	
    	if(!this.isUBN(_player)){
    		var vid = _player.id(),
    		_con = this.getContainerId();
    		
    		function resizeVideoJS(){
    			try{
    				var _width = document.getElementById(_con).offsetWidth;
                    var _height = _width * 9 / 16;
                    
                    _player.width(_width);
                    _player.height(_height);
    			}catch(err) {
    				console.log(err);
    			}
                
            }
    		resizeVideoJS();
            window.onresize = resizeVideoJS; 
    	}
    };
    
    JTPlayer.prototype.getPlayerContainer = function () {    	
    	if(this.player)
    		return this.player;
    };

	JTPlayer.prototype.getContainerId = function () {    	
    	return this.getValue('containerId');
    };
    
    JTPlayer.prototype.getPlayerId = function () {    	
    	return this.getValue('playerId');
    };
    
    JTPlayer.prototype.getPlayerSrc = function () {    	
    	return this.getValue('src');
    };
    
    JTPlayer.prototype.getPoster = function () {    	
    	return this.getValue('poster');
    };
    
    JTPlayer.prototype.getTrack = function () {    	
    	return this.getValue('track');
    };
    
    JTPlayer.prototype.getAd = function () {    	
    	return this.getValue('ad');
    };
    
    JTPlayer.prototype.isDRM = function () {    	
    	var _drm = this.getValue('drm');
    	if(_drm != null)
    		return true;
    };
    
    JTPlayer.prototype.getConfigFairPlay = function () { 
    	var _config = {};
    	
		var _drm = this.getValue('drm');
		if(!this.isUBN(_drm['fairplay'])){
			var _src = (_drm['fairplay']['src'] != undefined)? _drm['fairplay']['src'] : null,
			_license_url = (_drm['fairplay']['licenseurl'] != undefined)? _drm['fairplay']['licenseurl'] : null,
			_certificate_uri = (_drm['fairplay']['certificateurl'] != undefined)? _drm['fairplay']['certificateurl'] : null,
			_headers = (_drm['fairplay']['headers'] != undefined)? this.getDRMHeaders(_drm['fairplay']['headers']) : null;
			
			if(_src != null){
				_config['content_url'] = _src;
			}
			
			if(_license_url != null){
				_config['license_url'] = _license_url;
			}
			
			if(_certificate_uri != null){
				_config['certificate_uri'] = _certificate_uri;
			}
			
			if(_headers != null){
				_config['custom_header'] = _headers;
			}
		}
		
		return _config;
    	
    };
    
    JTPlayer.prototype.getConfigDRMPlay = function () {    	
    	var _config = { src:'', type:'', keySystems:{} };
    	
		var _drm = this.getValue('drm');
		if(!this.isUBN(_drm['widevine'])){
			var _src = (_drm['widevine']['src'] != undefined)? _drm['widevine']['src'] : null,
			_wlicense_url = (_drm['widevine']['licenseurl'] != undefined)? _drm['widevine']['licenseurl'] : null,
			_wheaders = (_drm['widevine']['headers'] != undefined)? this.getDRMHeaders(_drm['widevine']['headers']) : null;
			
			if(_src != null){
				_config['src'] = _src;
				_config['type'] = "application/dash+xml";
			}
			
			if(_wlicense_url != null && _wheaders != null){
				_config['keySystems']['com.widevine.alpha'] = {
 	                url: _wlicense_url,
 	                licenseHeaders: _wheaders,
 	            }
			}
		}
		
		if(!this.isUBN(_drm['playready'])){
			var _src = (_drm['playready']['src'] != undefined)? _drm['playready']['src'] : null,
			_plicense_url = (_drm['playready']['licenseurl'] != undefined)? _drm['playready']['licenseurl'] : null,
			_pheaders = (_drm['playready']['headers'] != undefined)? this.getDRMHeaders(_drm['playready']['headers']) : null;
			
			if(_src != null){
				_config['src'] = _src;
				_config['type'] = "application/dash+xml";
			}
			
			if(_plicense_url != null && _pheaders != null){
			   if(_plicense_url != null && _pheaders != null){
					_config['keySystems']['com.microsoft.playready'] = {
	 	                url: _plicense_url,
	 	                licenseHeaders: _pheaders,
	 	            }
			   }
			}
		}
		
		return _config;
    };
    
    JTPlayer.prototype.getConfigAllPlay = function () {    	
    	var _src = this.getPlayerSrc();
    	
    	var _config = {};
    	if(!this.isUBN(_src)){
    		_config['src'] = _src;
			_config['type'] = "application/x-mpegURL";
    	}
    	
    	return _config;
    	
    };
    
    JTPlayer.prototype.getDRMHeaders = function (_h) {  
    	
    	var _hd = {};
    	if(!this.isUBN(_h)){
    		if(typeof _h == 'object'){
    			if(Array.isArray(_h)){
    				for (var _k in _h) {
    					
    					_hd[_h[_k]['name']] = _h[_k]['value'];
    				}
    			}else{
    				_hd[_h['name']] = _h['value'];
    			}
    		}
    	}
    	
    	return _hd;
    };
	
	JTPlayer.prototype.getTimestamp = function () {    	
    	return new Date().getTime();
    };

	JTPlayer.prototype.getValue = function (key) {
		var _this = this;

		if(!_this.isUBN(key) && !_this.isUN(_this.options[key])){
			return _this.options[key];
		}else{
			return null;
		}
	};

	JTPlayer.prototype.setValue = function (key, value) {
		var _this = this;

		if(!_this.isUBN(key)){
			_this.options[key] = value;
		}
	};

	JTPlayer.prototype.createCookie = function (name, value, hours) {
        if (hours) {
            var date = new Date();
            date.setTime(date.getTime() + (hours * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        } else
            var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    };

    JTPlayer.prototype.readCookie = function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ')
                c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0)
                return c.substring(nameEQ.length, c.length);
        }
        return null;
    };

    JTPlayer.prototype.eraseCookie = function (name) {
        this.createCookie(name, "", -1);
    };
   

	JTPlayer.prototype.isUndefine = function (vval) {
        if (vval !== undefined) {
            return false;
        } else {
            return true;
        }
    };

    JTPlayer.prototype.isNull = function (vval) {
        if (vval !== null) {
            return false;
        } else {
            return true;
        }
    };

    JTPlayer.prototype.isBlank = function (vval) {
        if (vval.trim() !== "") {
            return false;
        } else {
            return true;
        }
    };

    JTPlayer.prototype.isUBN = function (vval) {
        if (vval !== undefined && vval !== null && vval !== '') {
            return false;
        } else {
            return true;
        }
    };

    JTPlayer.prototype.isUB = function (vval) {
        if (vval !== undefined && vval !== '') {
            return false;
        } else {
            return true;
        }
    };

    JTPlayer.prototype.isUN = function (vval) {
        if (vval !== undefined && vval !== null) {
            return false;
        } else {
            return true;
        }
    };
    JTPlayer.prototype.secondsToHms = function (vval) {
        var ft="";
        if (!this.isUBN(vval)) {
            var vval = Number(Math.round(vval));
            var h = Math.floor(vval / 3600);
            var m = Math.floor(vval % 3600 / 60);
            var s = Math.floor(vval % 3600 % 60);

            var hDisplay = h > 0 ? h  : "0";
            var mDisplay = m > 0 ? (m>9? m: "0"+m)  : "00";
            var sDisplay = s > 0 ? (s>9? s: "0"+s) : "00";
            ft = hDisplay +":"+ mDisplay +":"+ sDisplay; 
        }
        return ft;
    };

    var jtplayer = function(options) {
        
		if(typeof options == 'object'){
			return new JTPlayer(options);
		}else{
			throw new Error("Player options not set");
		}
        
    }; 


    window.jtplayer = jtplayer;

})));