(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('video.js')) :
	typeof define === 'function' && define.amd ? define(['video.js'], factory) :
	(global.videojsHttpSourceSelector = factory(global.videojs));
}(this, (function (videojs) { 'use strict';

videojs = videojs && videojs.hasOwnProperty('default') ? videojs['default'] : videojs;

var version = "1.1.5";

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};


var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};

var Component = videojs.getComponent('Component');

var CastMenuButton = function (_Component) {
	inherits(CastMenuButton, _Component);
  
  function CastMenuButton(player, options) {
    classCallCheck(this, CastMenuButton);
  }

  CastMenuButton.prototype.createEl = function createEl() {
	  return videojs.dom.createEl('google-cast-launcher', {
	      className: 'vjs-menu-button vjs-menu-button-popup vjs-control vjs-button'
	    });
  };
  
  return CastMenuButton;
}(Component);

// Default options for the plugin.
var defaults = {};

// Cross-compatibility for Video.js 5 and 6.
var registerPlugin = videojs.registerPlugin || videojs.plugin;
// const dom = videojs.dom || videojs;


var onPlayerReady = function onPlayerReady(player, options) {
  player.addClass('vjs-chrome-cast');
  
  //This plugin only supports level selection for HLS playback
  if (player.techName_ != 'Html5') {
    return false;
  }

  /**
  *
  * We have to wait for the manifest to load before we can scan renditions for resolutions/bitrates to populate selections
  *
  **/
  player.on(['loadedmetadata'], function (e) {
    
    // hack for plugin idempodency... prevents duplicate menubuttons from being inserted into the player if multiple player.httpSourceSelector() functions called.
    if (player.videojs_chrome_cast_initialized == 'undefined' || player.videojs_chrome_cast_initialized == true) {
      console.log("player.videojs_chrome_cast_initialized == true");
    } else {
      console.log("player.videojs_chrome_cast_initialized == false");
      player.videojs_chrome_cast_initialized = true;
      var controlBar = player.controlBar,
          fullscreenToggle = controlBar.getChild('fullscreenToggle').el();
      controlBar.el().insertBefore(controlBar.addChild('CastMenuButton').el(), fullscreenToggle);
    }
  });
};


var vjschromecast = function vjschromecast(options) {
  var player = this;

  this.ready(function () {
    //onPlayerReady(_this, videojs.mergeOptions(defaults, options));
	  if (player.videojs_chrome_cast_initialized == 'undefined' || player.videojs_chrome_cast_initialized == true) {
		    console.log("player.videojs_chrome_cast_initialized == true");
		  } else {
		    console.log("player.videojs_chrome_cast_initialized == false");
		    player.videojs_chrome_cast_initialized = true;
		    var controlBar = player.controlBar,
		        fullscreenToggle = controlBar.getChild('fullscreenToggle').el();
		    //controlBar.el().insertBefore(controlBar.addChild('CastMenuButton').el(), fullscreenToggle);
		    controlBar.addChild('button', {'id': 'vjs-chrome-cast', 'text':'sdfdsfsd'});
		  }
  });

  //videojs.registerComponent('CastMenuButton', CastMenuButton);
  
//hack for plugin idempodency... prevents duplicate menubuttons from being inserted into the player if multiple player.httpSourceSelector() functions called.
  
};

// Register the plugin with video.js.
registerPlugin('vjschromecast', vjschromecast);

// Include the version number.
vjschromecast.VERSION = version;

return vjschromecast;

})));